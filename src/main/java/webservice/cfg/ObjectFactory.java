
package webservice.cfg;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.cfg package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.cfg
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI002 }
	 * 
	 */
	public TABLEOFZPPTCI002 createTABLEOFZPPTCI002() {
		return new TABLEOFZPPTCI002();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP002_Type }
	 * 
	 */
	public ZPPTCSAP002_Type createZPPTCSAP002_Type() {
		return new ZPPTCSAP002_Type();
	}

	/**
	 * Create an instance of {@link ZPPTCI002 }
	 * 
	 */
	public ZPPTCI002 createZPPTCI002() {
		return new ZPPTCI002();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP002Response }
	 * 
	 */
	public ZPPTCSAP002Response createZPPTCSAP002Response() {
		return new ZPPTCSAP002Response();
	}

	/**
	 * Create an instance of {@link ZPPTCO002 }
	 * 
	 */
	public ZPPTCO002 createZPPTCO002() {
		return new ZPPTCO002();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO002 }
	 * 
	 */
	public TABLEOFZPPTCO002 createTABLEOFZPPTCO002() {
		return new TABLEOFZPPTCO002();
	}

}
