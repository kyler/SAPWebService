package webservice;

import java.net.PasswordAuthentication;
import java.util.Properties;

import webservice.helper.PropertyLoader;

public class Author {
	private final static PropertyLoader LOADER = new PropertyLoader();
	private final static Properties properties = LOADER.getProperties();
	private static PasswordAuthentication author;

	static {
		java.net.Authenticator.setDefault(new java.net.Authenticator() {
			@Override
			protected java.net.PasswordAuthentication getPasswordAuthentication() {
				author = new java.net.PasswordAuthentication(properties.getProperty("username"),
						properties.getProperty("password").toCharArray());
				return author;
			}
		});
	}

	public Author() {
	}
}
