
package webservice.classify;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.classify package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.classify
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ZPPTCSAP003Response }
	 * 
	 */
	public ZPPTCSAP003Response createZPPTCSAP003Response() {
		return new ZPPTCSAP003Response();
	}

	/**
	 * Create an instance of {@link ZPPTCO003 }
	 * 
	 */
	public ZPPTCO003 createZPPTCO003() {
		return new ZPPTCO003();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO003 }
	 * 
	 */
	public TABLEOFZPPTCO003 createTABLEOFZPPTCO003() {
		return new TABLEOFZPPTCO003();
	}

	/**
	 * Create an instance of {@link ZPPTCI003 }
	 * 
	 */
	public ZPPTCI003 createZPPTCI003() {
		return new ZPPTCI003();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI003 }
	 * 
	 */
	public TABLEOFZPPTCI003 createTABLEOFZPPTCI003() {
		return new TABLEOFZPPTCI003();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP003_Type }
	 * 
	 */
	public ZPPTCSAP003_Type createZPPTCSAP003_Type() {
		return new ZPPTCSAP003_Type();
	}

}
