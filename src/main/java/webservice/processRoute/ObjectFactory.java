
package webservice.processRoute;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.processRoute package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.processRoute
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO006 }
	 * 
	 */
	public TABLEOFZPPTCO006 createTABLEOFZPPTCO006() {
		return new TABLEOFZPPTCO006();
	}

	/**
	 * Create an instance of {@link ZPPTCI006 }
	 * 
	 */
	public ZPPTCI006 createZPPTCI006() {
		return new ZPPTCI006();
	}

	/**
	 * Create an instance of {@link ZPPTCO006 }
	 * 
	 */
	public ZPPTCO006 createZPPTCO006() {
		return new ZPPTCO006();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP006_Type }
	 * 
	 */
	public ZPPTCSAP006_Type createZPPTCSAP006_Type() {
		return new ZPPTCSAP006_Type();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP006Response }
	 * 
	 */
	public ZPPTCSAP006Response createZPPTCSAP006Response() {
		return new ZPPTCSAP006Response();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI006 }
	 * 
	 */
	public TABLEOFZPPTCI006 createTABLEOFZPPTCI006() {
		return new TABLEOFZPPTCI006();
	}

}
