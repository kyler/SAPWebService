
package webservice.options;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.options package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.options
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ZPPTCSAP004Response }
	 * 
	 */
	public ZPPTCSAP004Response createZPPTCSAP004Response() {
		return new ZPPTCSAP004Response();
	}

	/**
	 * Create an instance of {@link ZPPTCO004 }
	 * 
	 */
	public ZPPTCO004 createZPPTCO004() {
		return new ZPPTCO004();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO004 }
	 * 
	 */
	public TABLEOFZPPTCO004 createTABLEOFZPPTCO004() {
		return new TABLEOFZPPTCO004();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP004_Type }
	 * 
	 */
	public ZPPTCSAP004_Type createZPPTCSAP004_Type() {
		return new ZPPTCSAP004_Type();
	}

	/**
	 * Create an instance of {@link ZPPTCI004 }
	 * 
	 */
	public ZPPTCI004 createZPPTCI004() {
		return new ZPPTCI004();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI004 }
	 * 
	 */
	public TABLEOFZPPTCI004 createTABLEOFZPPTCI004() {
		return new TABLEOFZPPTCI004();
	}

}
