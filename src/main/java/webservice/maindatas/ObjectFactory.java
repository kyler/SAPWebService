
package webservice.maindatas;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.sap package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.sap
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ZPPTCSAP001_Type }
	 * 
	 */
	public ZPPTCSAP001_Type createZPPTCSAP001_Type() {
		return new ZPPTCSAP001_Type();
	}

	/**
	 * Create an instance of {@link ZPPTCO001 }
	 * 
	 */
	public ZPPTCO001 createZPPTCO001() {
		return new ZPPTCO001();
	}

	/**
	 * Create an instance of {@link ZPPTCI001 }
	 * 
	 */
	public ZPPTCI001 createZPPTCI001() {
		return new ZPPTCI001();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI001 }
	 * 
	 */
	public TABLEOFZPPTCI001 createTABLEOFZPPTCI001() {
		return new TABLEOFZPPTCI001();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP001Response }
	 * 
	 */
	public ZPPTCSAP001Response createZPPTCSAP001Response() {
		return new ZPPTCSAP001Response();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO001 }
	 * 
	 */
	public TABLEOFZPPTCO001 createTABLEOFZPPTCO001() {
		return new TABLEOFZPPTCO001();
	}

}
