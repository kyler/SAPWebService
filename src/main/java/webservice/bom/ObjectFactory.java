
package webservice.bom;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.bom package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.bom
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ZPPTCSAP005Response }
	 * 
	 */
	public ZPPTCSAP005Response createZPPTCSAP005Response() {
		return new ZPPTCSAP005Response();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI005 }
	 * 
	 */
	public TABLEOFZPPTCI005 createTABLEOFZPPTCI005() {
		return new TABLEOFZPPTCI005();
	}

	/**
	 * Create an instance of {@link ZPPTCI005 }
	 * 
	 */
	public ZPPTCI005 createZPPTCI005() {
		return new ZPPTCI005();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO005 }
	 * 
	 */
	public TABLEOFZPPTCO005 createTABLEOFZPPTCO005() {
		return new TABLEOFZPPTCO005();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP005_Type }
	 * 
	 */
	public ZPPTCSAP005_Type createZPPTCSAP005_Type() {
		return new ZPPTCSAP005_Type();
	}

	/**
	 * Create an instance of {@link ZPPTCO005 }
	 * 
	 */
	public ZPPTCO005 createZPPTCO005() {
		return new ZPPTCO005();
	}

}
