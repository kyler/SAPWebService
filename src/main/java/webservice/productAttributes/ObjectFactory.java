
package webservice.productAttributes;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the webservice.productAttributes package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: webservice.productAttributes
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ZPPTCO007 }
	 * 
	 */
	public ZPPTCO007 createZPPTCO007() {
		return new ZPPTCO007();
	}

	/**
	 * Create an instance of {@link ZPPTCI007 }
	 * 
	 */
	public ZPPTCI007 createZPPTCI007() {
		return new ZPPTCI007();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP007_Type }
	 * 
	 */
	public ZPPTCSAP007_Type createZPPTCSAP007_Type() {
		return new ZPPTCSAP007_Type();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCO007 }
	 * 
	 */
	public TABLEOFZPPTCO007 createTABLEOFZPPTCO007() {
		return new TABLEOFZPPTCO007();
	}

	/**
	 * Create an instance of {@link TABLEOFZPPTCI007 }
	 * 
	 */
	public TABLEOFZPPTCI007 createTABLEOFZPPTCI007() {
		return new TABLEOFZPPTCI007();
	}

	/**
	 * Create an instance of {@link ZPPTCSAP007Response }
	 * 
	 */
	public ZPPTCSAP007Response createZPPTCSAP007Response() {
		return new ZPPTCSAP007Response();
	}

}
