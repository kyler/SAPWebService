package untitled1;

import javax.xml.ws.Holder;

import webservice.Author;
import webservice.cfg.TABLEOFZPPTCI002;
import webservice.cfg.TABLEOFZPPTCO002;
import webservice.cfg.ZPPTCI002;
import webservice.cfg.ZPPTCO002;
import webservice.cfg.ZPPTCSAP002;
import webservice.cfg.ZPPTCSAP002_Service;
import webservice.classify.TABLEOFZPPTCI003;
import webservice.classify.TABLEOFZPPTCO003;
import webservice.classify.ZPPTCI003;
import webservice.classify.ZPPTCSAP003;
import webservice.classify.ZPPTCSAP003_Service;
import webservice.maindatas.TABLEOFZPPTCI001;
import webservice.maindatas.TABLEOFZPPTCO001;
import webservice.maindatas.ZPPTCI001;
import webservice.maindatas.ZPPTCO001;
import webservice.maindatas.ZPPTCSAP001;
import webservice.maindatas.ZPPTCSAP001_Service;

public class Test1 extends Author {
	Author author = new Author();

	public static void main(String[] args) {
		// testMainDatas();
		testCfg();
		testClassify();
	}

	private static void testClassify() {
		ZPPTCSAP003_Service service = new ZPPTCSAP003_Service();
		ZPPTCSAP003 zpptcsap003 = service.getZPPTCSAP003();
		Holder<TABLEOFZPPTCI003> input = new Holder<TABLEOFZPPTCI003>();
		TABLEOFZPPTCI003 tableofzpptci003=new TABLEOFZPPTCI003();
		ZPPTCI003 zpptci003=new ZPPTCI003();
		zpptci003.setZATWRT("");
		
		
		tableofzpptci003.getItem().add(zpptci003);
		input.value=tableofzpptci003;
		
		Holder<TABLEOFZPPTCO003> output = new Holder<TABLEOFZPPTCO003>();
		zpptcsap003.zppTCSAP003(input, output);
	}

	/**
	 * 测试配置信息传输
	 */
	private static void testCfg() {
		ZPPTCSAP002_Service service = new ZPPTCSAP002_Service();
		ZPPTCSAP002 zpptcsap002 = service.getZPPTCSAP002();
		Holder<TABLEOFZPPTCI002> input = new Holder<TABLEOFZPPTCI002>();
		TABLEOFZPPTCI002 tableofzpptci002 = new TABLEOFZPPTCI002();
		ZPPTCI002 zpptci002 = new ZPPTCI002();
		zpptci002.setZPACKNO("123456");// 数据包号
		zpptci002.setZITEM("123dsa");// 行号
		zpptci002.setZACTIONID("A");// 动作
		zpptci002.setZATNAM("AL001");
		zpptci002.setZATBEZ("特性描述");
		zpptci002.setZATWRT("AA00");
		zpptci002.setZATWTB("特性值描述");
		tableofzpptci002.getItem().add(zpptci002);
		input.value = tableofzpptci002;

		Holder<TABLEOFZPPTCO002> output = new Holder<TABLEOFZPPTCO002>();
		TABLEOFZPPTCO002 tableofzpptco002 = new TABLEOFZPPTCO002();
		ZPPTCO002 zpptco002 = new ZPPTCO002();
		tableofzpptco002.getItem().add(zpptco002);
		output.value = tableofzpptco002;

		zpptcsap002.zppTCSAP002(input, output);
		System.out.println(output.value.getItem().get(0).getTYPE());
	}

	/**
	 * 测试主数据传输
	 */
	private static void testMainDatas() {
		ZPPTCSAP001_Service service = new ZPPTCSAP001_Service();
		ZPPTCSAP001 zpptcsap001 = service.getZPPTCSAP001();
		Holder<TABLEOFZPPTCI001> input = new Holder<TABLEOFZPPTCI001>();
		TABLEOFZPPTCI001 tableofzpptci001 = new TABLEOFZPPTCI001();
		ZPPTCI001 zpptci001 = new ZPPTCI001();
		zpptci001.setRESERVE01("dsada");
		zpptci001.setGUID("S001");
		zpptci001.setZMATNR("S001SADA1231345");
		zpptci001.setZACTIONID("A");
		zpptci001.setZMAKTX("物料中文描述");
		zpptci001.setZMEINS("EA");
		zpptci001.setZMTART("A006");
		zpptci001.setZMRPC("Z01");
		zpptci001.setZBESKZ("E");
		zpptci001.setZMATKL("SADasD");
		tableofzpptci001.getItem().add(zpptci001);

		input.value = tableofzpptci001;// .getItem().add(zpptci001);

		Holder<TABLEOFZPPTCO001> output = new Holder<TABLEOFZPPTCO001>();
		TABLEOFZPPTCO001 tableofzpptco001 = new TABLEOFZPPTCO001();
		ZPPTCO001 zpptco001 = new ZPPTCO001();
		output.value = tableofzpptco001;// .getItem().add(zpptco001);

		zpptcsap001.zppTCSAP001(input, output);
		System.out.println(output.value.getItem().get(0).getMESSAGE());
		System.out.println(output.value.getItem().get(0).getTYPE());
	}
}
